# Reproduce inverted order of kafka -> telegraf -> Influx

Clone this repository and `cd` into the folder.

    docker-compose up --build
    
If the telegraf container crashes, start it up after the Kafka broker is completely ready (`docker start <telegrafcontainer>`)

Run the script to produce data to the Kafka source topic:

    ./produce_to_kafka.sh
    
This will produce 2 messages to the Kafka broker _(timestamp of now)_, which will be written to influx buffered (by telegraf).

Now check the result in influx:

    curl -X GET 'http://localhost:8086/query?pretty=true&db=raw&rp=autogen&q=SELECT%20%22active_power%22%20FROM%20%22PLANT%22'

You will see that the value for the `active_power` measurement saved in Influx is the value from the oldest kafka message (4300) 
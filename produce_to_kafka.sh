TS="$(date +%s)000000000"

echo $TS

docker run --interactive \
           --network telegraf-reproducer-net \
           confluentinc/cp-kafkacat \
            kafkacat -b broker:9092 \
                    -t input.raw \
                    -K: \
                    -P <<EOF
1:PLANT,id=a active_power=4300 $TS
1:PLANT,id=a active_power=5600 $TS
EOF
